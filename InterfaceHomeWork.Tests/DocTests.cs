using InterfaceHomeWork;
using NUnit.Framework;
using Moq;
using System;

namespace InterfaceHomeWork.Tests
{
    public class DocTests
    {
        [Test]
        public void RepoTest()
        {
            Doc newDoc = new Doc()
            {
                Title = "� ������������ ��������� ��������",
                Num = "196-��",
                Status = "�����������",
                Type = new string[] { "����������� �����", "����������� �������� ���" },
                DocDate = new DocDate { Accept = new DateTime(1995, 12, 10), Approval = new DateTime(1995, 12, 11), Entry = new DateTime(1995, 12, 26) }
            };
            Mock<IRepository<Doc>> mock = new Mock<IRepository<Doc>>();
            mock.Setup(x => x.GetAll());
            mock.Setup(x => x.GetOne(It.IsAny<Func<Doc, bool>>()));
            mock.Setup(x => x.Add(It.IsAny<Doc>()));
            var docService = new DocService(mock.Object);
            docService.AddDoc(newDoc);
            mock.Verify(x => x.Add(It.IsAny<Doc>()));
            mock.Verify(x => x.Add(It.IsAny<Doc>()), Times.Once());
        }
    }
}