﻿using System;

namespace InterfaceHomeWork
{
    [Serializable]
    public class DocDate
    {
        /// <summary>
        /// Дата принятия документа.
        /// </summary>
        public DateTime Accept { get; set; }

        /// <summary>
        /// Дата утверждения документа.
        /// </summary>
        public DateTime Approval { get; set; }

        /// <summary>
        /// Дата вступления в силу документа.
        /// </summary>
        public DateTime Entry { get; set; }

        public DocDate()
        { }
    }
}
