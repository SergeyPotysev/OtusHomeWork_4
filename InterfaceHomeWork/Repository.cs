﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace InterfaceHomeWork
{
    public class Repository : IRepository<Doc>
    {
        private readonly string _reposFile;
        private readonly SystemXml serializer = new SystemXml();

        public Repository()
        {
            string xmlDir = Path.Combine(Directory.GetCurrentDirectory(), "Xml");
            if (!Directory.Exists(xmlDir))
                Directory.CreateDirectory(xmlDir);
            _reposFile = Path.Combine(xmlDir, "repository.xml");
            if (File.Exists(_reposFile))
                File.Delete(_reposFile);
            Docs docs = new Docs();
            serializer.Serialize(docs, _reposFile);
        }

        public IEnumerable<Doc> GetAll()
        {
            return GetRepo();
        }


        public void Add(Doc doc)
        {
            IEnumerable<Doc> repDocs = GetRepo();
            repDocs = repDocs.Append(doc);
            Docs updatedRepo = new Docs(repDocs.ToArray());
            serializer.Serialize(updatedRepo, _reposFile);
        }

        public IEnumerable<Doc> GetRepo()
        {
            if (File.Exists(_reposFile))
            {
                Docs repDocs = serializer.Deserialize<Docs>(_reposFile);
                return repDocs.Documents;
            }
            else
                return Enumerable.Empty<Doc>();
        }

        public Doc GetOne(Func<Doc, bool> predicate)
        {
            IEnumerable<Doc> repDocs = GetRepo();
            var d = repDocs.FirstOrDefault(predicate);
            return d;
        }
    }
}
