﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace InterfaceHomeWork
{
    class ExtendedXml : ISerializer
    {
        public T Deserialize<T>(string data)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer().Create();
            return serializer.Deserialize<T>(new XmlReaderSettings { IgnoreWhitespace = false }, data);
        }

        public void Serialize<T>(T objGraph, string xmlFile)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer().Create();   
            string contents = serializer.Serialize(new XmlWriterSettings { Indent = true }, objGraph);
            File.WriteAllText(xmlFile, contents);
            Console.WriteLine("=> Saved docs in XML by ExtendedXmlSerializer");
        }
    }
}
