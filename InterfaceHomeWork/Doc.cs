﻿using System;

namespace InterfaceHomeWork
{
    [Serializable]
    public class Doc
    {
        /// <summary>
        /// Наименование документа.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Тип документа.
        /// </summary>
        public string[] Type { get; set; }

        /// <summary>
        /// Номер документа.
        /// </summary>
        public string Num { get; set; }

        /// <summary>
        /// Даты документа.
        /// </summary>
        public DocDate DocDate;

        /// <summary>
        /// Статус действия документа.
        /// </summary>
        public string Status { get; set; }

        public Doc()
        { } 
    }


}
