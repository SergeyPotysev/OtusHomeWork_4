﻿namespace InterfaceHomeWork
{
    public interface ISerializer
    {
        void Serialize<T>(T item, string xmlFile);
        T Deserialize<T>(string xmlFile);
    }
}