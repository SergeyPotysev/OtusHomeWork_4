﻿using System;
using System.Xml.Serialization;

namespace InterfaceHomeWork
{
    [XmlRoot("InterfaceHomeWork")]
    [Serializable]
    public class Docs
    {       
        public Doc[] Documents { get; set; }
        
        public Docs(Array newDocs)
        {
            Array.Copy(newDocs, Documents = new Doc[newDocs.Length], newDocs.Length);
        }

        public Docs()  
        {
            Documents = new Doc[] {
                new Doc()
                {
                    Title = "Фотограмметрия. Требования к созданию ориентированных аэроснимков",
                    Num = "58854-2020",
                    Status = "Не вступил в силу",
                    Type = new string[] { "ГОСТ Р", "Приказ Росстандарта" },
                    DocDate = new DocDate { Accept = new DateTime(2020, 5, 15), Approval = new DateTime(2020, 6, 12), Entry = new DateTime(2020, 12, 1) }
                },
                new Doc
                {
                    Title = "Смесители и краны водоразборные. Типы и основные размеры",
                    Num = "25809-2019",
                    Status = "Действующий",
                    Type = new string[] { "ГОСТ", "Приказ Росстандарта" },
                    DocDate = new DocDate { Accept = new DateTime(2019, 11, 19), Approval = new DateTime(2020, 1, 1), Entry = new DateTime(2020, 6, 1) }
                },
                new Doc
                {
                    Title = "Масло коровье. Технические условия",
                    Num = "37-91",
                    Status = "Недействующий",
                    Type = new string[] { "ГОСТ" },
                    DocDate = new DocDate { Accept = new DateTime(1991, 2, 25), Approval = new DateTime(1991, 2, 25), Entry = new DateTime(1992, 1, 1) }
                },
                new Doc
                {
                    Title = "Угли Восточной Сибири. Классификация",
                    Num = "9477-86",
                    Status = "Недействующий",
                    Type = new string[] { "ГОСТ" },
                    DocDate = new DocDate { Accept = new DateTime(1986, 12, 19), Approval = new DateTime(1987, 1, 1), Entry = new DateTime(1989, 1, 1) }
                },
                new Doc
                {
                    Title = "Электропоезда. Общие технические требования",
                    Num = "55434-2013",
                    Status = "Действующий. Отменен в части",
                    Type = new string[] { "ГОСТ Р", "Приказ Росстандарта" },
                    DocDate = new DocDate { Accept = new DateTime(2013, 5, 21), Approval = new DateTime(2014, 4, 3), Entry = new DateTime(2014, 1, 1) }
                },
                new Doc
                {
                    Title = "Арматура контактной сети железной дороги линейная",
                    Num = "12393-2013",
                    Status = "Применение в качестве национального стандарта РФ прекращено",
                    Type = new string[] { "ГОСТ", "Приказ Росстандарта", "Межгосударственный стандарт" },
                    DocDate = new DocDate { Accept = new DateTime(2014, 4, 29), Approval = new DateTime(2014, 10, 27), Entry = new DateTime(2014, 7, 1) }
                },
                new Doc
                {
                    Title = "Пожарная безопасность зданий и сооружений",
                    Num = "21-01-97*",
                    Status = "Действующий",
                    Type = new string[] { "СНиП", "Свод правил" },
                    DocDate = new DocDate { Accept = new DateTime(1997, 2, 13), Approval = new DateTime(1997, 2, 13), Entry = new DateTime(1998, 1, 1) }
                }
            };
        }
    }
}
