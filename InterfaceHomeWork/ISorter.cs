﻿using System.Collections.Generic;

namespace InterfaceHomeWork
{
    public interface ISorter<T> 
    {
       IEnumerable<T> Sort(IEnumerable<T> notSortedDocs);
    }
}
