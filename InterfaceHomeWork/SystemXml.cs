﻿using ExtendedXmlSerializer.ContentModel;
using System;
using System.IO;
using System.Xml.Serialization;

namespace InterfaceHomeWork
{
    class SystemXml : ISerializer
    {
        public void Serialize<T>(T objGraph, string xmlFile)
        {  
            XmlSerializer serializer = new XmlSerializer(objGraph.GetType()); 
            using (Stream fStream = new FileStream(xmlFile, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                serializer.Serialize(fStream, objGraph); 
            }
            Console.WriteLine("=> Saved docs in XML by System.Xml.Serialization");
        }

        public T Deserialize<T>(string xmlFile)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (Stream reader = new FileStream(xmlFile, FileMode.Open))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
     }
}