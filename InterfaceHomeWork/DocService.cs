﻿namespace InterfaceHomeWork
{
    public class DocService : IDocService
    {
        public IRepository<Doc> repository;

        public DocService(IRepository<Doc> repository)
        {
            this.repository = repository;
        }

        public void AddDoc(Doc document)
        {
            if (!string.IsNullOrEmpty(document.Title) && (document.Type.Length > 0) && !string.IsNullOrEmpty(document.Num) 
                 && (document.DocDate.Accept != default) && (document.DocDate.Approval != default) && (document.DocDate.Entry != default)
                 && (!string.IsNullOrEmpty(document.Status)))
            {
                repository.Add(document);
            }
        }
    }
}
