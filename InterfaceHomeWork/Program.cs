﻿using System;
using System.Collections.Generic;
using System.IO;

namespace InterfaceHomeWork
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string xmlDir = Path.Combine(Directory.GetCurrentDirectory(), "Xml");
            if (!Directory.Exists(xmlDir))
                Directory.CreateDirectory(xmlDir);
            string sXmlFile = Path.Combine(xmlDir, "system_docs.xml");
            string eXmlFile = Path.Combine(xmlDir, "extend_docs.xml");
            Docs docs = new Docs();       

            // Использование сериализатора System.Xml.Serialization.
            SystemXml serializer_1 = new SystemXml();
            serializer_1.Serialize(docs, sXmlFile);

            // Использование сериализатора ExtendedXmlSerializer.
            ExtendedXml serializer_2 = new ExtendedXml();
            serializer_2.Serialize(docs, eXmlFile);

            // Использование десериализатора System.Xml.Serialization.
            List<Doc> system_docs = new List<Doc>();
            Docs deserialized_1 = serializer_1.Deserialize<Docs>(sXmlFile);

            foreach (Doc item in deserialized_1.Documents)
                system_docs.Add(item);
            Console.WriteLine("<= Received docs from XML by System.Xml.Serialization");

            // Использование десериализатора ExtendedXmlSerializer.
            List<Doc> extended_docs = new List<Doc>();
            foreach (Docs deserialized_2 in new MyStreamReader<Docs>(eXmlFile, serializer_2))
            {
                foreach (Doc item in deserialized_2.Documents)
                    extended_docs.Add(item);
            }
            Console.WriteLine("<= Received docs from XML by ExtendedXmlSerializer");

            // Отбор по статусу "Действующий" и сортировка по дате начала действия документа.
            DocSorter order = new DocSorter();
            var sorted_docs = order.Sort(extended_docs);

            // Вывод на консоль списка документов после отбора и сортировки. 
            int i = 0;
            Console.WriteLine("\nДействующие документы, отсортированные по дате начала действия\n" + "" + "==============================================================");
                                    
            foreach (var docItem in sorted_docs)
            {
                Console.WriteLine(i+1 + ". Наименование: " + docItem.Title);
                Console.WriteLine("   Вид документа:");
                foreach (string type in docItem.Type)
                {
                    Console.Write("\t" + type + "\n");
                }
                Console.WriteLine("   Номер: " + docItem.Num);
                Console.WriteLine("   Статус: " + docItem.Status);
                Console.WriteLine("   Дата принятия: " + docItem.DocDate.Accept.ToString("d"));
                Console.WriteLine("   Дата утверждения: " + docItem.DocDate.Approval.ToString("d"));
                Console.WriteLine("   Дата начала действия: " + docItem.DocDate.Entry.ToString("d"));
                Console.WriteLine("   -----------------------------------------------------------");
                ++i;
            }
            // Реализация интерфейса IRepository 
            Repository repo = new Repository();
            Doc newDoc = new Doc()
            {
                Title = "О безопасности дорожного движения",
                Num = "196-ФЗ",
                Status = "Действующий",
                Type = new string[] { "Федеральный закон", "Нормативный правовой акт" },
                DocDate = new DocDate { Accept = new DateTime(1995, 12, 10), Approval = new DateTime(1995, 12, 11), Entry = new DateTime(1995, 12, 26) }
            };
            // Получить все документы из репозитория
            var allDocs = repo.GetAll();
            // Получить документ с нужным текстом в наименовании
            string titleContent = "Классификация";
            var neededDoc = repo.GetOne(x => x.Title.IndexOf(titleContent) >= 0);
            // Добавить документ в репозиторий
            repo.Add(newDoc);
            Console.ReadKey();
        }
    }
}
