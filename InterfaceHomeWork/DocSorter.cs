﻿using System.Collections.Generic;
using System.Linq;

namespace InterfaceHomeWork
{
    class DocSorter : ISorter<Doc>
    {
        public IEnumerable<Doc> Sort(IEnumerable<Doc> notSortedDocs)
        {     
            return notSortedDocs.Where(x => x.Status.IndexOf("Действующий") >= 0).OrderBy(x => x.DocDate.Entry);
        }
    }
}
