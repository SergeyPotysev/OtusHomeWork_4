﻿using System;
using System.Collections;
using System.IO;

namespace InterfaceHomeWork
{
    public class MyStreamReader<T> : IEnumerable, IDisposable
    {
        private readonly string data;
        private readonly ISerializer serializer;
        private bool isDisposed = false;

        public MyStreamReader(string xmlFile, ISerializer serializer)
        {
            data = File.ReadAllText(xmlFile); ;
            this.serializer = serializer;
        }

        ~MyStreamReader()
        {
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                }
                isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {            
            yield return serializer.Deserialize<T>(data);
        }
    }
}
